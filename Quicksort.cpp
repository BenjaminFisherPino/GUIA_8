#include <iostream>
using namespace std;

#include "Quicksort.h"

/*CONSTRUCTOR*/
Quicksort::Quicksort(int *array){
  this->array = array;
}

/*METODO QUICKSORT*/
void Quicksort::quicksort(int n) {
  int tope, ini, fin, pos;
  int izq, der, aux, band;
  
  tope = 0;
  int pilamenor[n];
  int pilamayor[n];
  
  pilamenor[0] = 0;
  pilamayor[0] = n;

  while (tope > -1) {
    ini = pilamenor[tope];
    fin = pilamayor[tope];
    tope = tope - 1;
    
    // reduce
    izq = ini;
    der = fin;
    pos = ini;
    band = true;
    
    while (band == true) {
      while ((this->array[pos] <= this->array[der]) && (pos != der))
        der = der - 1;
        
      if (pos == der) {
        band = false;
      } else {
        aux = this->array[pos];
        this->array[pos] = this->array[der];
        this->array[der] = aux;
        pos = der;
        
        while ((this->array[pos] >= this->array[izq]) && (pos != izq))
          izq = izq + 1;
          
        if (pos == izq) {
          band = false;
        } else {
          aux = this->array[pos];
          this->array[pos] = this->array[izq];
          this->array[izq] = aux;
          pos = izq;
        }
      }
    }
    
    if (ini < (pos - 1)) {
      tope = tope + 1;
      pilamenor[tope] = ini;
      pilamayor[tope] = pos - 1;
    }
    
    if (fin > (pos + 1)) {
      tope = tope + 1;
      pilamenor[tope] = pos + 1;
      pilamayor[tope] = fin;
    }
  }
}

/*METODO IMPRIMIR VECTOR*/
void Quicksort::imprimir_vector(int n, int VER){
  int i, cont;
  cont = 0;
  if (VER == 1){//Solo si la opcion esta habilitada

    for (i=0; i< n; i++) {
      cout << " A[" << i + 1 <<"] = " << this->array[i] << " ";
      cont++;

      if (cont == 4){
        cont = 0;
        cout << endl;
      }
    }
    cout << endl << endl;
  }
}