#ifndef SELECCION_H
#define SELECCION_H

class Seleccion {

    /*ATRIBUTOS*/
    private:
        int *array;

    public:

    /*CONSTRUCTOR*/
		  Seleccion(int *array);

    /*METODOS*/
        void imprimir_vector(int n, int VER);
        void ordena_seleccion(int n);

};
#endif
