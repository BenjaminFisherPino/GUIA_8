#include <iostream>
#include <fstream>
#include <chrono>

#include "Seleccion.h"
#include "Quicksort.h"

using namespace std;

/*FUINCION MAIN*/
int main(int argc, char **argv) {
	
    /*VARIABLES*/
	int n, VER, temp;
    string text;
	
	n = atoi(argv[1]); //Tamaño del arreglo
    VER = atoi(argv[2]); //Opcion para mostrar el arreglo

    int array_q[n]; // Array Quicksort
    int array_s[n]; // Array Seleccion

    /*VALIDACION DE PARAMETROS DE ENTRADA*/
	if (n <= 0 ) { 
		cout << "Debe ingresar un numero mayor a 0" << endl;
        while (n <= 0){
            cin >> n;
        }
        system("clear");
	} 

    if (VER == 1){
        text = "TRUE";
    } else if (VER == 2){
        text = "FALSE";
    }

    /*LLENADO DEL ARRAY*/
    srand((unsigned) time(0));
    for (int i = 0; i < n; i++) {
        temp = (rand() % 35) + 1;
        array_q[i] = temp; // Array Quicksort
        array_s[i] = temp; // Array Seleccion
    }

    system("clear");

    /*CONFIGURACIONES INICIALES*/
    Quicksort q = Quicksort(array_q);
    Seleccion s = Seleccion(array_s);

    q.imprimir_vector(n, VER);

    cout << "*****************************************" << endl;
    cout << "               MENU                      " << endl;
    cout << "                                         " << endl;
    cout << "→ TAMAÑO DE LA LISTA: " << n << "        " << endl;
    cout << "→ IMPRESION DE RESULTADOS: " << text << "" << endl;
    cout << "                                         " << endl;
    cout << "*****************************************" << endl;
    cout << endl << endl;

    // Calculo del tiempo del algoritmo de Seleccion
    auto start_s = chrono::steady_clock::now(); // Inicio
    s.ordena_seleccion(n);
    auto end_s = std::chrono::steady_clock::now(); // Fin

    // Tiempo que tarda el algoritmo de seleccion
    chrono::duration<double> elapsed_seconds_s = end_s - start_s;

    // Calculo del tiempo del algoritmo de Quicksort
    auto start_q = chrono::steady_clock::now(); // Inicio
    q.quicksort(n);
    auto end_q = std::chrono::steady_clock::now(); // Fin

    // Tiempo que tarda el algoritmo de Quicksort
    chrono::duration<double> elapsed_seconds_q = end_q - start_q;

    // Impresion de los resultados obtenidos
    cout << "-----------------------------------------" << endl;
    cout << "Metodo    |Tiempo" << endl;
    cout << "-----------------------------------------" << endl;
    cout << "Seleccion |" << elapsed_seconds_s.count() * 1000 << " milisegundos" << endl;
    cout << "Quicksort |" << elapsed_seconds_q.count() * 1000 << " milisegundos" << endl;
    cout << "-----------------------------------------" << endl << endl;

    q.imprimir_vector(n, VER);

    cout << endl << "Presione 'ENTER' para salir" << endl;

    getchar();
    system("clear");
	return 0;
}
