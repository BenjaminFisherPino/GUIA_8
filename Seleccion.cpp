#include <iostream>
using namespace std;

#include "Seleccion.h"

/*CONSTRUCTOR*/
Seleccion::Seleccion(int *array){
  this->array = array;
}

/*METODO IMPRIME VECTOR*/
void Seleccion::imprimir_vector(int n, int VER) {
  int i, cont;
  cont = 0;
  if (VER == 1){//Solo si la opcion esta habilitada

    for (i=0; i< n; i++) {
      cout << " A[" << i + 1 <<"] = " << this->array[i] << " ";
      cont++;

      if (cont == 4){
        cont = 0;
        cout << endl;
      }
    }
    cout << endl << endl;
  }
}

/*METODO DE ORDENAMIENTO POR SELECCION*/
void Seleccion::ordena_seleccion(int n) {
  int i, menor, k, j;
  
  for (i=0; i<=n-2; i++) {
    menor = this->array[i];
    k = i;
    
    for (j=i+1; j<=n-1; j++) {
      if (this->array[j] < menor) {
        menor = this->array[j];
        k = j;
      }
    }
    
    this->array[k] = this->array[i];
    this->array[i] = menor;
  }
}