#ifndef QUICKSORT_H
#define QUICKSORT_H

class Quicksort {

    /*ATRIBUTOS*/
    private:
      int *array;
      
    public:

    /*CONSTRUCTOR*/
		  Quicksort(int *array);

    /*METODOS*/
        void imprimir_vector(int n, int VER);
        void quicksort(int n);

};
#endif
