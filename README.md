# Guia 8

**Versión 1.0**

Archivo README.md para guía numero 8 de Algoritmos y Estructuras de Datos

Profesor: Alejandro Mauricio Valdes Jimenez

Asignatura: Algoritmos y Estructuras de Datos

Autor: Benjamin Ignacio Fisher Pino

Fecha: 29/11/2021

---
## Resúmen del programa

Este programa fue hecho por Benjamin Fisher para visualizar la diferencia de tiempos en la tarea de ordenar un array de tamaño N por dos algoritmos distintos, algoritmo de "orden por seleccion" y "Quicksort" y sacar concluciones respecto a en que caso es mas util la implementacion de cada algoritmo.

---
## Requerimientos

Para utilizar este programa se requiere un computador con sistema operativo Linux Ubuntu version 20.04 L, un compilador g++ version 9.3.0 y la version 4.2.1 de make. En caso de no tener el compilador de g++ o make y no saber como descargarlos, abajo se encuentra una pequeña guia de como descargarlos.

### Instalacion de g++

Para instalar g++ debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install g++"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para compilar codigos de c y c++.

### Instalacion de make

Para instalar make debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install make"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para poder generar un ejecutable del programa.

---
## Como instalarlo

Para descargar los archivos debe dirigirse al repositorio alocado en el siguiente enlace.

Link: https://gitlab.com/BenjaminFisherPino/GUIA_8.git

Tras ingresar al repositorio debe proceder a clonarlo ó a descargar los archivos manualmente, ambas opciones funcionan.Luego debe dirigirse a travez de su terminal a la ruta donde se encuentran los archivos descargados. Una vez ahi, usted debe ingresar a la carpeta  y escribir el siguiente comando en su terminal.

"make" y posteriormente "./proyecto N M"

→ EJEMPLO

"./proyecto 20 1"

N corresponde a un numero entero mayor a 2, el cual designa el tamaño de nuestra matriz, mientras que M es un entero que nos permite mostrar los arrays (previo al ordenamiento y posterior al ordenamiento). Si M es igual a 1, se mostraran los arrays, en caso contrario, se omitira esto y solo se mostraran los tiempos de ejecucion de cada algoritmo. Es vital la utilizacion de estos parametros para el funcionamiento del programa. ;)

En caso de tener problemas asegurece de tener la version 9.3.0 de g++ y  la version 4.2.1 de make

Puede revisar con "g++ --version" y "make --version" respectivamente.

Si siguio todos los pasos debiese estar listo para ejecutar el programa!

---
## Funcionamiento del programa

EL programa fue desarrollado para poder ver los tiempos que tardan en ordenar un array de tamaño N dos algoritmos, el de "orden por seleccion" y "Quicksort" y sacar conclusiones respecto a los contextos en los cuales se puede implementar cada algoritmo. El programa esta construido en base a una funcion main, "Main.cpp", mas dos clases que nos permiten aplicar los algoritmos de ordenamiento de datos. El codigo funciona con tres funciones principales:

1)"quicksort": Esta funcion nos permite aplicar el algoritmo de quicksort a un array

2)"orden_seleccion": Esta funcion nos permite aplicar el algoritmo de orden por seleccion a un array

3)"imprimir_vector": Nos permite imprimir el array previ a el ordenamiento y posterior a este.

Cabe destacar la utilizacion de la libreria "chrono" de c++, la cual nos permite calcular el tiempo que se demora  cada algoritmo y guardar este tiempo en una variable para posteriormente imprimirlo por pantalla.

Antes de iniciar el programa, se debe introducir el tamaño del array y si el usuario quiere ver el array previo y post ordenamiento (Si no esta seguro de como hacerlo, revise en el apartado de instalacion). Tras iniciado el programa, con sus respectivas validaciones, se generara dos arrays identicos tamaño N con elementos aleatorios, luego se utilizan los algoritmos previamente nombrados y se les toma el tiempo (gracias a la libreria chrono de c++) que se demoraron en realizar la accion de ordenar estos arrays de forma creciente. Finalmente se imprimen los resultados obtenidos y se finaliza con la ejecucion del programa.

---

## License & Copyright

Ⓒ Benjamín Fisher Pino, Universidad de Talca

→ Mail: bfisher20@alumnos.utalca.cl

